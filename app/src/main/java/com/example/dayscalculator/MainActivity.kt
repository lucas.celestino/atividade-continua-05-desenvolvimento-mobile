package com.example.dayscalculator

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.app.DatePickerDialog
import android.widget.*
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {
    private lateinit var startDateButton: Button
    private lateinit var endDateButton: Button
    private lateinit var addButton: Button
    private lateinit var resultTextView: TextView

    private var startDate: Date? = null
    private var endDate: Date? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startDateButton = findViewById(R.id.start_date_button)
        endDateButton = findViewById(R.id.end_date_button)
        addButton = findViewById(R.id.add_button)
        resultTextView = findViewById(R.id.result_text_view)

        startDateButton.setOnClickListener {
            showDatePickerDialog { date ->
                startDate = date
                startDateButton.text = formatDate(date)
            }
        }

        endDateButton.setOnClickListener {
            showDatePickerDialog { date ->
                endDate = date
                endDateButton.text = formatDate(date)
            }
        }

        addButton.setOnClickListener {
            if (startDate == null || endDate == null) {
                Toast.makeText(this, "Selecione a data de início e fim primeiro", Toast.LENGTH_SHORT).show()
            } else {
                val dateDifference = DateCalculator.calculateDateDifference(startDate!!, endDate!!)
                val result = "Diferença: ${dateDifference.days} dias, ${dateDifference.hours} horas, " +
                        "${dateDifference.minutes} minutos, ${dateDifference.seconds} segundos"
                resultTextView.text = result
            }
        }
    }

    private fun showDatePickerDialog(onDateSelected: (Date) -> Unit) {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(
            this,
            { _, selectedYear, selectedMonth, selectedDay ->
                val selectedCalendar = Calendar.getInstance()
                selectedCalendar.set(selectedYear, selectedMonth, selectedDay)
                onDateSelected(selectedCalendar.time)
            },
            year,
            month,
            day
        )
        datePickerDialog.show()
    }

    private fun formatDate(date: Date): String {
        val format = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        return format.format(date)
    }
}
