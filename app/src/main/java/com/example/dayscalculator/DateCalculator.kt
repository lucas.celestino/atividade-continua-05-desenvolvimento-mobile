package com.example.dayscalculator

import java.util.*

class DateCalculator {
    companion object {
        private const val WORKING_DAYS_PER_WEEK = 5

        fun calculateDateDifference(startDate: Date, endDate: Date): DateDifference {
            val diffInMillis = endDate.time - startDate.time
            val diffInSeconds = diffInMillis / 1000
            val diffInMinutes = diffInSeconds / 60
            val diffInHours = diffInMinutes / 60
            val diffInDays = diffInHours / 24

            return DateDifference(diffInDays, diffInHours % 24, diffInMinutes % 60, diffInSeconds % 60)
        }

        fun addDaysToDate(date: Date, numDays: Int, isWorkingDays: Boolean): Date {
            val calendar = Calendar.getInstance()
            calendar.time = date

            var daysToAdd = numDays
            if (isWorkingDays) {
                val numWeeks = daysToAdd / WORKING_DAYS_PER_WEEK
                val remainingDays = daysToAdd % WORKING_DAYS_PER_WEEK

                // Adjust for weekends
                val currentDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)
                val daysToAddIncludingWeekends = currentDayOfWeek + remainingDays

                if (daysToAddIncludingWeekends > WORKING_DAYS_PER_WEEK) {
                    daysToAdd += 2  // Skip the weekend
                } else if (currentDayOfWeek + remainingDays == Calendar.SATURDAY) {
                    daysToAdd += 2  // Skip the weekend
                } else if (currentDayOfWeek + remainingDays > Calendar.SATURDAY) {
                    daysToAdd++  // Skip Sunday
                }

                daysToAdd += numWeeks * 2  // Account for weekends in full weeks
            }

            calendar.add(Calendar.DAY_OF_MONTH, daysToAdd)
            return calendar.time
        }
    }
}

data class DateDifference(val days: Long, val hours: Long, val minutes: Long, val seconds: Long)
